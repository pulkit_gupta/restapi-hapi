# Table of Contents #


# Steps #

### Prerequisite ##

- `NodeJs v4.x` and above required.
- `MongoDB Server 3.2.x` and above required.

## Installation ##

Available for **Node v4** and above. See the installation steps below:

### Download(GIT) ###
```bash
$ git clone git@bitbucket.org:pulkit_gupta/restapi-hapi.git
```
### NPM Install(npm) ###
```bash
$ npm install
$ node app.js (By Default you can see server running on Address http://localhost:9999)
```

### Explore the documentation and test it ###
Go to `http://localhost:999/documentation`.

## Things to improve ##

* Construct the models in class structures.
* Make the response models for the API.
* Redis support for auth.
* Whatever you wanna add.......
